+++
title = "About"
description = "Hugo, the world's fastest framework for building websites"
date = "2019-02-28"
aliases = ["about-us", "about-hugo", "contact"]
author = "Hugo Authors"
+++

## Learning

* Kubernetes
* Containers
* Terraform
* Python
* Shell Script
* Powershell
* Cloud platforms, AWS, Azure, GCP
* CI/CD
* Ansible
* Ansible Molecule
* Puppet
* Vagrant
* Vault
* Packer
* Observability (ELK Stack,Graylog,Grafana,Prometheus)
* Google AppScripts
* ITSM



